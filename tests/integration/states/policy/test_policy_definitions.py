import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_policy_definition(hub, ctx):
    """
    This test provisions a policy definition, describes policy definition and deletes the provisioned policy definition.
    """
    # Create policy definition
    pd_name = "idem-test-location-policy-definition-" + str(int(time.time()))
    pd_parameters = {
        "policy_type": "Custom",
        "mode": "Indexed",
        "display_name": "test-location-policy-definition-" + str(int(time.time())),
        "description": "Idem location policy definition for testing",
        "policy_rule": {
            "if": {
                "not": {"field": "location", "in": "[parameters('allowedLocations')]"}
            },
            "then": {"effect": "deny"},
        },
        "parameters": {
            "allowedLocations": {
                "type": "Array",
                "metadata": {
                    "displayName": "Allowed locations",
                    "description": "The list of locations that can be specified when deploying the resources",
                    "strongType": "location",
                },
            },
        },
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create policy definition with --test
    pd_ret = await hub.states.azure.policy.policy_definitions.present(
        test_ctx,
        name=pd_name,
        policy_definition_name=pd_name,
        **pd_parameters,
    )
    assert pd_ret["result"], pd_ret["comment"]
    assert not pd_ret["old_state"] and pd_ret["new_state"]
    assert (
        f"Would create azure.policy.policy_definitions '{pd_name}'" in pd_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=pd_ret["new_state"],
        expected_old_state=None,
        expected_new_state=pd_parameters,
        policy_definition_name=pd_name,
        idem_resource_name=pd_name,
    )
    resource_id = pd_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{pd_name}"
        == resource_id
    )

    # Create policy definition in real
    pd_ret = await hub.states.azure.policy.policy_definitions.present(
        ctx,
        name=pd_name,
        policy_definition_name=pd_name,
        **pd_parameters,
    )
    assert pd_ret["result"], pd_ret["comment"]
    assert not pd_ret["old_state"] and pd_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=pd_ret["new_state"],
        expected_old_state=None,
        expected_new_state=pd_parameters,
        policy_definition_name=pd_name,
        idem_resource_name=pd_name,
    )
    resource_id = pd_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{pd_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-06-01",
        retry_count=5,
        retry_period=10,
    )

    # Describe policy definition
    describe_ret = await hub.states.azure.policy.policy_definitions.describe(ctx)
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.policy.policy_definitions.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        policy_definition_name=pd_name,
        idem_resource_name=resource_id,
    )

    pd_update_parameters = {
        "policy_type": "Custom",
        "mode": "Indexed",
        "display_name": "test-location-updated-policy-definition-"
        + str(int(time.time())),
        "description": "Idem location policy definition updated for testing",
        "policy_rule": {
            "if": {
                "not": {"field": "location", "in": "[parameters('allowedLocations')]"}
            },
            "then": {"effect": "deny"},
        },
        "parameters": {
            "allowedLocations": {
                "type": "Array",
                "metadata": {
                    "displayName": "Allowed locations",
                    "description": "The list of locations that can be specified when deploying the resources",
                    "strongType": "location",
                },
            },
        },
    }
    # Update policy definition with --test
    pd_ret = await hub.states.azure.policy.policy_definitions.present(
        test_ctx,
        name=pd_name,
        policy_definition_name=pd_name,
        **pd_update_parameters,
    )
    assert pd_ret["result"], pd_ret["comment"]
    assert pd_ret["old_state"] and pd_ret["new_state"]
    assert (
        f"Would update azure.policy.policy_definitions '{pd_name}'" in pd_ret["comment"]
    )
    check_returned_states(
        old_state=pd_ret["old_state"],
        new_state=pd_ret["new_state"],
        expected_old_state=pd_parameters,
        expected_new_state=pd_update_parameters,
        policy_definition_name=pd_name,
        idem_resource_name=pd_name,
    )
    resource_id = pd_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{pd_name}"
        == resource_id
    )

    # Update policy definition in real
    pd_ret = await hub.states.azure.policy.policy_definitions.present(
        ctx,
        name=pd_name,
        policy_definition_name=pd_name,
        **pd_update_parameters,
    )
    assert pd_ret["result"], pd_ret["comment"]
    assert pd_ret["old_state"] and pd_ret["new_state"]
    assert f"Updated azure.policy.policy_definitions '{pd_name}'" in pd_ret["comment"]
    check_returned_states(
        old_state=pd_ret["old_state"],
        new_state=pd_ret["new_state"],
        expected_old_state=pd_parameters,
        expected_new_state=pd_update_parameters,
        policy_definition_name=pd_name,
        idem_resource_name=pd_name,
    )
    resource_id = pd_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/providers/Microsoft.Authorization/policyDefinitions/{pd_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-06-01",
        retry_count=5,
        retry_period=10,
    )

    # Delete policy definition with --test
    pd_del_ret = await hub.states.azure.policy.policy_definitions.absent(
        test_ctx,
        name=pd_name,
        policy_definition_name=pd_name,
    )
    assert pd_del_ret["result"], pd_del_ret["comment"]
    assert pd_del_ret["old_state"] and not pd_del_ret["new_state"]
    assert (
        f"Would delete azure.policy.policy_definitions '{pd_name}'"
        in pd_del_ret["comment"]
    )
    check_returned_states(
        old_state=pd_del_ret["old_state"],
        new_state=None,
        expected_old_state=pd_update_parameters,
        expected_new_state=None,
        policy_definition_name=pd_name,
        idem_resource_name=pd_name,
    )

    # Delete policy definitions
    pd_del_ret = await hub.states.azure.policy.policy_definitions.absent(
        ctx,
        name=pd_name,
        policy_definition_name=pd_name,
    )
    assert pd_del_ret["result"], pd_del_ret["comment"]
    assert pd_del_ret["old_state"] and not pd_del_ret["new_state"]
    assert (
        f"Deleted azure.policy.policy_definitions '{pd_name}'" in pd_del_ret["comment"]
    )
    check_returned_states(
        old_state=pd_del_ret["old_state"],
        new_state=None,
        expected_old_state=pd_update_parameters,
        expected_new_state=None,
        policy_definition_name=pd_name,
        idem_resource_name=pd_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-06-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete policy definition again
    pd_del_ret = await hub.states.azure.policy.policy_definitions.absent(
        ctx,
        name=pd_name,
        policy_definition_name=pd_name,
    )
    assert pd_del_ret["result"], pd_del_ret["comment"]
    assert not pd_del_ret["old_state"] and not pd_del_ret["new_state"]
    assert (
        f"azure.policy.policy_definitions '{pd_name}' already absent"
        in pd_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    policy_definition_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert policy_definition_name == old_state.get("policy_definition_name")
        assert expected_old_state["policy_type"] == old_state.get("policy_type")
        assert expected_old_state["mode"] == old_state.get("mode")
        assert expected_old_state["display_name"] == old_state.get("display_name")
        assert expected_old_state["description"] == old_state.get("description")
        assert expected_old_state["policy_rule"] == old_state.get("policy_rule")
        assert expected_old_state["parameters"] == old_state.get("parameters")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert policy_definition_name == new_state.get("policy_definition_name")
        assert expected_new_state["policy_type"] == new_state.get("policy_type")
        assert expected_new_state["mode"] == new_state.get("mode")
        assert expected_new_state["display_name"] == new_state.get("display_name")
        assert expected_new_state["description"] == new_state.get("description")
        assert expected_new_state["policy_rule"] == new_state.get("policy_rule")
        assert expected_new_state["parameters"] == new_state.get("parameters")
